let messagesLoaded = false;
let weaponForPlayerInfo;
let vehicleForPlayerInfo;
let adminPanelsHistory = new Array();
var addedWeaponTypes = new Array();
var addedVehicleTypes = new Array();

$(function () {
    var Accordion = function(el, multiple) {
      this.el = el || {};
      // more then one submenu open?
      this.multiple = multiple || false;
      
      var dropdownlink = this.el.find('.dropdownlink');
      dropdownlink.on('click',
                      { el: this.el, multiple: this.multiple },
                      this.dropdown);
    };
    
    Accordion.prototype.dropdown = function(e) {
      var $el = e.data.el,
          $this = $(this),
          //this is the ul.submenuItems
          $next = $this.next();
      
      $next.slideToggle();
      $this.parent().toggleClass('open');
      
      if(!e.data.multiple) {
        //show only one menu at the same time
        $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
      }
    }
    
    var accordion = new Accordion($('.admin-menu-container'), false);
})

function logInfo(message) {
	let logContainer = document.getElementById("log");
	let existingElements = logContainer.childNodes;
	if (existingElements.length >= 5) {
	logContainer.removeChild(logContainer.firstChild);
	}
	let newElement = document.createElement("div");
	newElement.textContent = message;
	logContainer.appendChild(newElement);
	}

//do somthing when html document is ready
$(document).ready(function () {
	i18next.use(window.i18nextXHRBackend).init({
		backend: {
			loadPath: '../i18n/en.json'
		}
	}, function(err, t) {
		jqueryI18next.init(i18next, $);
		$(document).localize();
		messagesLoaded = true;
	});
	
	setAdminBackButtonAction();

	//Create panel for users list
	createUsersList();
});

function showHide(id, rememberPrevious) {	
	if (document.getElementById) {
	  	var divid = document.getElementById(id);
		var divs = document.getElementsByClassName("admin-menu-panel");
		let backButton = document.getElementById('admin-menu-back-button-id');

		if (id !== 'admin-menu-main-panel-id')
			backButton.style.display = 'block';
		else
			backButton.style.display = 'none';
		
		let visibleDivs = new Array();
		
		for (var i = 0; i < divs.length; i++) {
			if ($(divs[ i ]).is(":visible"))
				visibleDivs.push($(divs[ i ]));
		}

		for (let i = 0; i < visibleDivs.length; i++) {
			if (i == visibleDivs.length - 1) {
				if (rememberPrevious === true)
					adminPanelsHistory.push($(visibleDivs[ i ]).attr('id'));

				$(visibleDivs[ i ]).fadeOut("fast", function () { $(divid).fadeIn("slow"); });

				return true;
			}

			$(visibleDivs[ i ]).fadeOut("fast");
		}
			
	}

	return false;
}

function setAdminBackButtonAction() {
	let backButton = document.getElementById('admin-menu-back-button-id');

	backButton.onclick = function () {
		let panelToShow = adminPanelsHistory.pop();
		showHide(panelToShow, false);
	}
}

function createUsersList() {
	let adminMenuContainer = document.getElementById('admin-menu-container-id');

	let usersContainer = document.createElement('ul');
	usersContainer.id = "admin-menu-users-list-id";
	usersContainer.className = 'admin-menu-panel list-menu';

	adminMenuContainer.appendChild(usersContainer);
}

function setActionsForNumbersList(functionToExecute) {
	let adminNumbersList = document.getElementById('admin-menu-numbers-list-id');
	let buttons = adminNumbersList.getElementsByTagName("a");

	for (let i = 0; i < buttons.length; i++) {
		buttons[ i ].onclick = function () {
			functionToExecute(buttons[ i ].dataset.value);
		}
	}
}

function populateUsersList(usersJson, actionId) {
	let usersArray = JSON.parse(usersJson);
	let usersContainer = document.getElementById('admin-menu-users-list-id');

	//Clear users list
	while (usersContainer.firstChild) {
		usersContainer.removeChild(usersContainer.firstChild);
	}

	//Add users to list
	for (let i = 0; i < usersArray.length; i++) {
		let userContainer = document.createElement('li');
		let userButton = document.createElement('a');

		userButton.href = '#';
		userButton.onclick = function () {
			switch (actionId) {
				case "giveWeapon":
					//give player weapon
					mp.trigger('adminRequestGivePlayerWeapon', usersArray[ i ].playerId, weaponForPlayerInfo.hash, weaponForPlayerInfo.ammunition);
					break;
				case "giveVehicle":
					//give player weapon
					mp.trigger('adminRequestGivePlayerVehicle', usersArray[ i ].playerId, vehicleForPlayerInfo.hash);
					break;
			}
			
		};

		userButton.textContent = "[" + usersArray[ i ].playerId + "] " + usersArray[ i ].playerName;
		userContainer.appendChild(userButton);
		usersContainer.appendChild(userContainer);
	}
}
    
function populateAdminWeaponsList(weaponsJSON) {
	if (messagesLoaded) {
		let adminMenuContainer = document.getElementById('admin-menu-container-id');
		let weaponsArray = JSON.parse(weaponsJSON);
		let weaponsTypesList = document.getElementById('admin-weapons-types-list-id');

		//Populating weapons types admin list
		for (var i = 0; i < weaponsArray.length; i++) {

			if (addedWeaponTypes.indexOf(weaponsArray[ i ].type) == -1) {
				addedWeaponTypes.push(weaponsArray[ i ].type);

				let weaponTypeContainer = document.createElement('li');
				let typeButton = document.createElement('a');
				typeButton.href = '#';

				let weaponsListId = 'admin-menu-' + addedWeaponTypes.indexOf(weaponsArray[ i ].type) + '-weapons-list-id';

				typeButton.onclick = function () {
					showHide(weaponsListId, true);
				};

				typeButton.textContent = weaponsArray[ i ].type;

				weaponTypeContainer.appendChild(typeButton);
				weaponsTypesList.appendChild(weaponTypeContainer);

				let currentTypeWeaponsContainer = document.createElement('ul');
				currentTypeWeaponsContainer.id = weaponsListId;
				currentTypeWeaponsContainer.className = 'admin-menu-panel list-menu';

				adminMenuContainer.appendChild(currentTypeWeaponsContainer);
			}

			let weaponsContainer = document.getElementById('admin-menu-' + addedWeaponTypes.indexOf(weaponsArray[ i ].type) + '-weapons-list-id');
			let weaponContainer = document.createElement('li');
			let weaponButton = document.createElement('a');

			weaponButton.href = '#';
			weaponButton.setAttribute('data-hash', weaponsArray[ i ].weapon);
			weaponButton.textContent = weaponsArray[ i ].name + " " + weaponButton.dataset.hash;

			weaponButton.onclick = function () {
				weaponForPlayerInfo = new Object();
				weaponForPlayerInfo.hash = weaponButton.dataset.hash;

				setActionsForNumbersList(function (ammunition) {
					weaponForPlayerInfo.ammunition = ammunition;
					mp.trigger('adminRequestUsersList', "giveWeapon");
					showHide('admin-menu-users-list-id', true);
				});

				showHide('admin-menu-numbers-list-id', true);
			};
			
			weaponContainer.appendChild(weaponButton);
			weaponsContainer.appendChild(weaponContainer);
		}
	} else {
		setTimeout(function() { populateAdminWeaponsList(weaponsJSON); }, 100);
	}
}

function populateAdminVehiclesList(vehiclesJSON) {
	if (messagesLoaded) {
		let adminMenuContainer = document.getElementById('admin-menu-container-id');
		let vehiclesArray = JSON.parse(vehiclesJSON);
		let vehiclesTypesList = document.getElementById('admin-vehicles-types-list-id');

		//Populating weapons types admin list
		for (var i = 0; i < vehiclesArray.length; i++) {

			if (addedVehicleTypes.indexOf(vehiclesArray[ i ].type) == -1) {
				addedVehicleTypes.push(vehiclesArray[ i ].type);

				let vehicleTypeContainer = document.createElement('li');
				let typeButton = document.createElement('a');
				typeButton.href = '#';

				let vehiclesListId = 'admin-menu-' + addedVehicleTypes.indexOf(vehiclesArray[ i ].type) + '-vehicles-list-id';

				typeButton.onclick = function () {
					showHide(vehiclesListId, true);
				};

				typeButton.textContent = vehiclesArray[ i ].typeName;

				vehicleTypeContainer.appendChild(typeButton);
				vehiclesTypesList.appendChild(vehicleTypeContainer);

				let currentTypeVehiclesContainer = document.createElement('ul');
				currentTypeVehiclesContainer.id = vehiclesListId;
				currentTypeVehiclesContainer.className = 'admin-menu-panel list-menu';

				adminMenuContainer.appendChild(currentTypeVehiclesContainer);
			}

			let vehiclesContainer = document.getElementById('admin-menu-' + addedVehicleTypes.indexOf(vehiclesArray[ i ].type) + '-vehicles-list-id');
			let vehicleContainer = document.createElement('li');
			let vehicleButton = document.createElement('a');

			vehicleButton.href = '#';
			vehicleButton.setAttribute('data-hash', vehiclesArray[ i ].hash);
			vehicleButton.textContent = vehiclesArray[ i ].model + " " + vehicleButton.dataset.hash;

			vehicleButton.onclick = function () {
				vehicleForPlayerInfo = new Object();
				vehicleForPlayerInfo.hash = vehicleButton.dataset.hash;
				mp.trigger('adminRequestUsersList', "giveVehicle");
				showHide('admin-menu-users-list-id', true);
			};
			
			vehicleContainer.appendChild(vehicleButton);
			vehiclesContainer.appendChild(vehicleContainer);
		}
	} else {
		setTimeout(function() { populateAdminVehiclesList(vehiclesJSON); }, 100);
	}
}