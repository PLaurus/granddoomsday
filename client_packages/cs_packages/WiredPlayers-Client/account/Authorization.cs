﻿using RAGE;
using RAGE.Elements;
using System;
using System.Net.Mail;
using WiredPlayers_Client.globals;

namespace WiredPlayers_Client.account
{
    class Authorization : Events.Script
    {
        public Authorization()
        {
            Events.Add("showAuthorizationWindow", AccountLoginFormEvent);
            Events.Add("requestPlayerSignIn", RequestPlayerLoginEvent);
            Events.Add("showSignInError", ShowSignInErrorEvent);
            Events.Add("clearAuthorizationWindow", ClearAuthorizationWindowEvent);

            Events.Add("createPlayerAccount", CreatePlayerAccountEvent);
            Events.Add("showRegistrationError", ShowRegistrationErrorEvent);
        }

        public static void AccountLoginFormEvent(object[] args)
        {
            // Disable the chat
            Chat.Activate(true);//false
            Chat.Show(true);//false

            // Create login window
            Browser.CreateBrowserEvent(new object[] { "package://statics/html/login.html" });
        }

        private void RequestPlayerLoginEvent(object[] args)
        {
            // Get the login from the array
            string login = args[0].ToString();

            // Get the password from the array
            string password = args[1].ToString();

            // Check for the credentials
            Events.CallRemote("signInAccount", login, password);
        }

        private void ShowSignInErrorEvent(object[] args)
        {
            const int messageType = 3;
            const int messagePositionOnScreen = 6;
            const string message = "Неверный логин или пароль";
            const int duration = 3000;

            // Show the message on the panel
            Browser.ExecuteFunctionEvent(new object[] { "notify", messageType, messagePositionOnScreen, message, duration});
        }

        private void ClearAuthorizationWindowEvent(object[] args)
        {
            // Enable the chat
            Chat.Activate(true);
            Chat.Show(true);

            // Unfreeze the player
            Player.LocalPlayer.FreezePosition(false);

            // Show the message on the panel
            Browser.DestroyBrowserEvent(null);

            // Show the player as logged
            Globals.playerLogged = true;
        }

        private void CreatePlayerAccountEvent(object[] args)
        {
            // Get the login from the array
            string login = args[0].ToString();

            // Get the password from the array
            string password = args[1].ToString();

            // Get the password from the array
            string secondPassword = args[2].ToString();

            //Check registration attempt timeout
            if (!CanSendRegistrationData())
                return;

            // Create login window
            Events.CallRemote("registerAccount", login, password, secondPassword);
        }

        private void ShowRegistrationErrorEvent(object[] args)
        {
            const int messageType = 3;
            const int messagePositionOnScreen = 6;
            string message = args[0].ToString();
            const int duration = 3000;

            // Show the message on the panel
            Browser.ExecuteFunctionEvent(new object[] { "notify", messageType, messagePositionOnScreen, message, duration });
        }

        private DateTime lastAttemptTime = new DateTime();
        private const int TIMEOUT_SECONDS = 2;

        private bool CanSendRegistrationData()
        {
            DateTime currentTime = DateTime.Now;
            double difference = ((TimeSpan)(currentTime - lastAttemptTime)).TotalSeconds;

            if(difference < TIMEOUT_SECONDS)
            {
                const int messageType = 3;
                const int messagePositionOnScreen = 6;
                const string message = "Повторите попытку позже";
                const int duration = 3000;

                // Show the message on the panel
                Browser.ExecuteFunctionEvent(new object[] { "notify", messageType, messagePositionOnScreen, message, duration });
                return false;
            }
            else
            {
                lastAttemptTime = currentTime;
            }

            return true;
        
        }

    
    }
}

