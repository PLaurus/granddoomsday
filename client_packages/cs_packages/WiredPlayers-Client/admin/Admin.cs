﻿using RAGE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WiredPlayers_Client.globals;

namespace WiredPlayers_Client.admin
{
    class Admin : Events.Script
    {
        private enum DataType
        {
            WEAPONS = 0,
            VEHICLES = 1,
            ADMIN_VERIFICATION = 2
        }

        private string userButtonAction = string.Empty;

        private static bool adminMenuEnabled = false;

        private string adminUsersListJson = null;

        public Admin()
        {
            Events.Add("receivedAdminMenuData", ReceivedAdminMenuData);
            Events.Add("adminRequestUsersList", AdminRequestUsersList);
            Events.Add("receivedAdminMenuUsersList", ReceivedAdminMenuUsersList);
            Events.Add("adminRequestGivePlayerWeapon", AdminRequestGivePlayerWeapon);
            Events.Add("adminRequestGivePlayerVehicle", AdminRequestGivePlayerVehicle);
        }

        private void ReceivedAdminMenuData(object[] args)
        {
            if (args[0] == null)
                return;

            if (args[1] == null)
                return;

            Chat.Activate(false);

            switch (ObjectToDataType(args[0]))
            {
                case DataType.ADMIN_VERIFICATION:
                    bool isAdmin = (bool)args[1];

                    if (isAdmin && !adminMenuEnabled)
                    {
                        // Show the menu
                        Browser.CreateBrowserEvent(new object[] { "package://statics/html/administratorMenu.html" });
                        adminMenuEnabled = true;
                    }
                    break;
                case DataType.WEAPONS:
                    if (adminMenuEnabled)
                    {
                        Chat.Output("received weapons");
                        string adminWeaponsListJson = args[1].ToString();
                        Browser.ExecuteFunctionEvent(new object[] { "populateAdminWeaponsList", adminWeaponsListJson });
                    }
                    break;
                case DataType.VEHICLES:
                    if (adminMenuEnabled)
                    {
                        string adminVehiclesListJson = args[1].ToString();
                        Browser.ExecuteFunctionEvent(new object[] { "populateAdminVehiclesList", adminVehiclesListJson });
                    }
                    break;
            }
        }

        private void ReceivedAdminMenuUsersList(object[] args)
        {
            if (!adminMenuEnabled)
                return;

            if (args[0] == null)
                return;

            // Get the variables from the arguments
            adminUsersListJson = args[0].ToString();

            // Populate users list
            Browser.ExecuteFunctionEvent(new object[] { "populateUsersList", adminUsersListJson, userButtonAction });
        }

        private void AdminRequestUsersList(object[] args)
        {
            if (args[0] == null)
                return;

            userButtonAction = args[0].ToString();

            Events.CallRemote("requestAdminMenuUsersList");
        }

        private void AdminRequestGivePlayerWeapon(object[] args)
        {
            if (args.Length != 3)
                return;

            int playerId = (int)args[0];

            if (!uint.TryParse(args[1].ToString(), out uint weaponHash))
                return;

            if (!int.TryParse(args[2].ToString(), out int ammunition))
                return;

            Events.CallRemote("adminGivePlayerWeapon", playerId, weaponHash, ammunition);
        }

        private void AdminRequestGivePlayerVehicle(object[] args)
        {
            if (args.Length != 2)
                return;

            int playerId = (int)args[0];

            if (!uint.TryParse(args[1].ToString(), out uint vehicleHash))
                return;
            
            Events.CallRemote("adminGivePlayerVehicle", playerId, vehicleHash);
        }

        public static void ShowHideAdminMenu()
        {
            if (adminMenuEnabled)
            {
                Chat.Activate(true);
                Browser.DestroyBrowserEvent(null);
                adminMenuEnabled = false;
            }
            else
            {
                if (Browser.customBrowser != null)
                    return;

                Events.CallRemote("requestAdminMenuData");
            }
        }

        private static DataType ObjectToDataType(object obj)
        {
            DataType enumValue = (DataType)Enum.ToObject(typeof(DataType), obj);
            return enumValue;
        }
    }
}
