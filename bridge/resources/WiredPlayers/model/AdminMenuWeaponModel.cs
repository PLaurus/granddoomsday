﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace WiredPlayers.model
{
    class AdminMenuWeaponModel
    {
        public VehicleHash hash { get; set; }
        public int carShop { get; set; }
        public int type { get; set; }
        public int speed { get; set; }
        public int price { get; set; }
        public string model { get; set; }

        public AdminMenuWeaponModel(VehicleHash hash, int carShop, int type, int price)
        {
            this.hash = hash;
            this.carShop = carShop;
            this.type = type;
            this.price = price;
        }
    }
}
