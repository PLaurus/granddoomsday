﻿using GTANetworkAPI;
using System;
using WiredPlayers.globals;
using WiredPlayers.messages.vehicles;

namespace WiredPlayers.model
{
    public class CarShopVehicleModel
    {
        public VehicleHash hash { get; set; }
        public int carShop { get; set; }
        public VehicleType type { get; set; }
        public int price { get; set; }

        public string model { get; set; }
        public int speed { get; set; }

        public string typeName { get; set; }

        public CarShopVehicleModel(VehicleHash hash, int carShop, VehicleType type, int price)
        {
            this.hash = hash;
            this.carShop = carShop;
            this.type = type;
            this.price = price;

            model = this.hash.ToString();
            speed = (int)Math.Round(NAPI.Vehicle.GetVehicleMaxSpeed(this.hash) * 3.6f);

            typeName = getReadableType(type);

        }

        private string getReadableType(VehicleType type)
        {
            string result = "Undefined";

            switch (type)
            {
                case VehicleType.COMPACT_CARS:
                    result = VehicleTypeRes.compact_cars;
                    break;
                case VehicleType.SEDANS:
                    result = VehicleTypeRes.sedans;
                    break;
                case VehicleType.COUPE:
                    result = VehicleTypeRes.coupe;
                    break;
                case VehicleType.STATION_WAGONS:
                    result = VehicleTypeRes.station_wagons;
                    break;
                case VehicleType.MUSCLE_CARS:
                    result = VehicleTypeRes.muscle_cars;
                    break;
                case VehicleType.SPORTCARS:
                    result = VehicleTypeRes.sportcars;
                    break;
                case VehicleType.SUPERCARS:
                    result = VehicleTypeRes.supercars;
                    break;
                case VehicleType.CLASSIC_CARS:
                    result = VehicleTypeRes.classic_cars;
                    break;
                case VehicleType.SUV:
                    result = VehicleTypeRes.suv;
                    break;
                case VehicleType.OFF_ROAD:
                    result = VehicleTypeRes.off_road;
                    break;
                case VehicleType.VANS:
                    result = VehicleTypeRes.vans;
                    break;
                case VehicleType.TRUCKS:
                    result = VehicleTypeRes.trucks;
                    break;
                case VehicleType.UTILITY:
                    result = VehicleTypeRes.utility;
                    break;
                case VehicleType.SERVICE:
                    result = VehicleTypeRes.service;
                    break;
                case VehicleType.EMERGENCY:
                    result = VehicleTypeRes.emergency;
                    break;
                case VehicleType.MILITARY:
                    result = VehicleTypeRes.military;
                    break;
                case VehicleType.TRAILERS:
                    result = VehicleTypeRes.trailers;
                    break;
                case VehicleType.BOATS:
                    result = VehicleTypeRes.boats;
                    break;
                case VehicleType.PLANES:
                    result = VehicleTypeRes.planes;
                    break;
                case VehicleType.HELICOPTERS:
                    result = VehicleTypeRes.helicopters;
                    break;
                case VehicleType.CYCLES:
                    result = VehicleTypeRes.cycles;
                    break;
                case VehicleType.MOTORCYCLES:
                    result = VehicleTypeRes.motorcycles;
                    break;
                case VehicleType.TRAINS:
                    result = VehicleTypeRes.trains;
                    break;
                case VehicleType.INDUSTRIAL:
                    result = VehicleTypeRes.industrial;
                    break;
            }

            return result;
        }
    }
}
