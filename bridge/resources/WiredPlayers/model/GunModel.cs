﻿using GTANetworkAPI;
using System;

namespace WiredPlayers.model
{
    public class GunModel
    {
        public WeaponHash weapon { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string ammunition { get; set; }
        public int capacity { get; set; }

        public GunModel(WeaponHash weapon, string name, string type, string ammunition, int capacity)
        {
            this.weapon = weapon;
            this.name = name;
            this.type = type;
            this.ammunition = ammunition;
            this.capacity = capacity;
        }
    }
}
